var express = require('express')
var router = express.Router()

var timestamps = require('../models/timestamps')

router.get('/selection', function(req, res) {

  var from = req.param('from', -1)
  var to = req.param('to', -1)

  timestamps.selection(from, to, function(err, docs) {
    res.render('timestamps', {timestamps: docs})
  })
})

router.get('/', function(req, res) {
  timestamps.all(function(err, docs) {
    res.render('timestamps', {timestamps: docs})
  })
})


module.exports = router
