var express = require('express')
var router = express.Router()

var graphs = require('../models/graphs')
var path = require("path");

var viewsDir = path.join(__dirname + '/../views')

router.get('/overview', function(req, res) {
  graphs.overview(function(err, docs) {
    res.sendFile(viewsDir + '/graphs.html');
  })
})



module.exports = router
