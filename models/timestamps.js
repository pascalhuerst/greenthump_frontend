var dp = require('../data-provider')

exports.selection = function(from, to, cb) {
  dp.getTimestamps(Number(from), Number(to), function(err, docs){
    cb(err, docs);
  });
}

exports.all = function(cb) {
  dp.getTimestamps(0, 0, function(err, docs){
    cb(err, docs);
  });
}
