var db = require('../db')

exports.overview = function(cb) {
  var collection = db.get().collection('jsontest')

  collection.find().toArray(function(err, docs) {
    cb(err, docs)
  })
}
