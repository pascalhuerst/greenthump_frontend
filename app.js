var express = require('express')
var app = express()
var db = require('./db')
var stylus = require('stylus')
var nib = require('nib')
var serveStatic = require('serve-static');

app.engine('jade', require('jade').__express)
app.set('view engine', 'jade')

app.use(serveStatic('assets'));
app.use('/timestamps', require('./controllers/timestamps'))
app.use('/graphs', require('./controllers/graphs'))

function compile(str, path) {
  return stylus(str)
    .set('filename', path)
    .use(nib());
}


app.use(stylus.middleware(
  { src: __dirname + '/assets'
  , compile: compile
  }
))


// Connect to Mongo on start
db.connect('mongodb://localhost:27017', function(err) {
  if (err) {
    console.log('Unable to connect to Mongo.')
    process.exit(1)
  } else {
    var a = app.listen(3000, function() {
      console.log('Connected to db. Listening on port 3000...')
    })


    var io = require('socket.io')(a);
    io.on('connection', function(socket){
      socket.on('dataRequest', function(msg){



        socket.emit('data', [1, 1, 2, 3, 3, 4, 3, 2, 1, 5, 6, 7, 8]);

        console.log('dataRequest: ' + msg);
      });
    });




  }
})
