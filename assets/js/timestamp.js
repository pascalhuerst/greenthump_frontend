var moment = require('moment');

exports.toTimestamp = function(datetime) {
  return moment(datetime).unix();
}

exports.toDatetime = function(timestamp) {
  return moment(timestamp * 1000).format('YYYY-MM-DD HH:mm:ss');
}
