var MongoClient = require('mongodb').MongoClient

var state = {
  db: null,
}

exports.connect = function(url, done) {
  if (state.db) {
    console.log('Already connected!')
    return done()
  }
  MongoClient.connect(url, function(err, db) {
    if (err) {
      console.log('connect error')
      return done(err)
    }
    console.log('setting db variable')
    state.db = db.db('testdb')
    done()
  })
}

exports.get = function() {
  console.log('get...')
  return state.db
}

exports.close = function(done) {
  if (state.db) {
    state.db.close(function(err, result) {
      state.db = null
      state.mode = null
      done(err)
    })
  }
}
